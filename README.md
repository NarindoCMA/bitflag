# bitflag #

`bitflag` is a collection of classes to manage state tracking, data attribute, software configuration, etc as boolean or integer values using a single 64bit integers, make them compact and fast to store in database or cache.

### Classes ###
- **BitFlag**  for booleans.
- **BitValue** for 0 based integers


### Examples (BitFlag) ###
```
from bitflag import BitFlag

FLAG = [
    'FL_CONFIG_ONE',
    'FL_CONFIG_TWO',
    'FL_ANOTHER_CONFIG',
    'FL_YET_ANOTHER',
    'FL_LAST_ONE',
]

bf = BitFlag(FLAG)

# To use existing data, e.g. from database or cache layer
# bf = BitFlag(FLAG, existing_value)

# To set or get a flag, there are 2 ways,
# 1. Attribute
bf.FL_CONFIG_ONE = True
bf.FL_ANOTHER_CONFIG = True
print(bf.FL_CONFIG_ONE)     # True

# 2. Dictionary
bf['FL_CONFIG_TWO'] = True
bf['FL_YET_ANOTHER'] = True
print(bf['FL_CONFIG_TWO'])  # True

# Get the long integer, for saving to db / cache
print(bf.as_int())          # 15

# Get the flags as dictionary
print(bf.as_dict())
# {
#    'FL_CONFIG_ONE': True,
#    'FL_CONFIG_TWO': True,
#    'FL_ANOTHER_CONFIG': True,
#    'FL_YET_ANOTHER': True,
#    'FL_LAST_ONE': False
# }
```
### Examples (BitValue) ###
```
from bitflag import BitValue

VALUE = {
    'FL_VALUE_ONE': 5,
    'FL_VALUE_TWO': 100,
    'FL_ANOTHER_VALUE': 7,
    'FL_YET_ANOTHER': 1000,
    'FL_LAST_ONE': 50,
}

bv = BitValue(VALUE)

# To use existing data, e.g. from database or cache layer
# bf = BitValue(VALUE, existing_value)

# To set or get a flag, there are 2 ways,
# 1. Attribute
bv.FL_VALUE_ONE = 2
bv.FL_ANOTHER_VALUE = 5
print(bv.FL_VALUE_ONE)     # 2

# 2. Dictionary
bv['FL_VALUE_TWO'] = 99
bv['FL_YET_ANOTHER'] = 850
print(bv['FL_VALUE_TWO'])  # 99

# Get the long integer, for saving to db / cache
print(bv.as_int())          # 6969114

# Get the values as dictionary
print(bv.as_dict())
# {
#   'FL_VALUE_ONE': 2,
#   'FL_VALUE_TWO': 99,
#   'FL_ANOTHER_VALUE': 5,
#   'FL_YET_ANOTHER': 850,
#   'FL_LAST_ONE': 0
# }
```

### Notes ###
The order of elements in `FLAG` or `VALUE` is crucial.
Don't change the order if there's a new element, just append it to the bottom.

### Critics & Suggestions ###
steve.zhan@narindo.com
