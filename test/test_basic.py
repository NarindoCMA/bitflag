import pytest

from .. basic import check, check_multi
from .. basic import newflag, addflag, addflag_multi
from .. basic import delflag, delflag_multi
from .. basic import bitcount, prepend_num, explode_num, implode_num
from .. basic import BIT, MAX

def test_BIT():
    assert BIT[0] == 1
    assert BIT[3] == 8
    assert BIT[63] == 1 << 63

def test_check():
    assert check(4, 4) == True
    assert check(5, 4) == True
    assert check(4, 1) == False
    assert check(11, 3) == True
    assert check(11, 4) == False
    assert check(11, (4,0,4)) == False

def test_check_multi():
    assert check_multi(11, [1,2]) == True
    assert check_multi(11, (4,0,4)) == False
    assert check_multi(11, (8,2)) == True

def test_newflag():
    assert newflag([1,2]) == 3
    assert newflag([1,2,1]) == 3
    assert newflag([2,0]) == 2
    assert newflag([1,2, 8 ]) == 11

def test_addflag():
    assert addflag(0, 1) == 1
    assert addflag(8, 8) == 8
    assert addflag(8, [2,4,8]) == 14

def test_addflag_multi():
    assert addflag_multi(8, [2,4,8]) == 14
    assert addflag_multi(2, [2,0,2]) == 2

def test_delflag():
    assert delflag(5, 1) == 4
    assert delflag(4, 1) == 4
    assert delflag(8, 1) == 8
    assert delflag(9, 1) == 8
    assert delflag(9, [2,4]) == 9
    assert delflag(9, [1,2,4]) == 8

def test_delflag_multi():
    assert delflag_multi(9, [2,4]) == 9
    assert delflag_multi(9, [1,2,4]) == 8
    assert delflag_multi(11, [1,2,4]) == 8

def test_bitcount():
    assert bitcount(1) == 1
    assert bitcount(4) == 3
    assert bitcount(250) == 8

def test_bitcount_error():
    with pytest.raises(TypeError):
        bitcount('dsads')
    with pytest.raises(TypeError):
        bitcount(1.5)

def test_prepend_num():
    assert prepend_num(0, 4, 7) == 7
    assert prepend_num(7, 1, 0) == 14
    assert prepend_num(7, 2, 1) == 29

def test_explode_num():
    assert explode_num(29, [2,4]) == [1, 7]
    assert explode_num(7, [4]) == [7]
    assert explode_num(14, [1,4]) == [0, 7]

def test_implode_num():
    assert implode_num([(2,3),(2,3),(2,3),(2,3)]) == 255
    assert implode_num([(5,0),(5,0),(5,0)]) == 0